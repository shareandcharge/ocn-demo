/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
const fetch = require('node-fetch');
const ethers = require('ethers');
const Notary = require('@shareandcharge/ocn-notary').default;
const BackendBase = require('./backend-base');

const cpoData = require('./cpo-data.json');

module.exports = class CpoBackend extends BackendBase {
  static get TOKEN_B() {
    return 'f3f1985e-8341-490d-ab06-17584175998c';
  }

  constructor(cpoInfo) {
    super(cpoInfo);
    this.initAppRoutes();
  }

  changeOwner(data) {
    const ownerData = {
      country_code: this.partyInfo.countryCode,
      party_id: this.partyInfo.partyID,
    };
    return Object.assign(data, ownerData);
  }

  static authorize(req, res, next) {
    if (req.headers.authorization !== `Token ${CpoBackend.TOKEN_B}`) {
      return res.send({
        status_code: 2001,
        timestamp: new Date(),
      });
    }
    next();
    return null;
  }

  static async signMessage(headers, params, body) {
    const privkey = ethers.Wallet.createRandom().privateKey;
    const notary = new Notary();
    await notary.sign({
      headers,
      params,
      body,
    }, privkey);
    return notary.serialize();
  }

  initAppRoutes() {
    this.app.get('/ocpi/versions', CpoBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: [{
          version: '2.2',
          url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/2.2`,
        }],
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/2.2', CpoBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: {
          version: '2.2',
          endpoints: [
            {
              identifier: 'locations',
              role: 'SENDER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/cpo/2.2/locations`,
            },
            {
              identifier: 'tariffs',
              role: 'SENDER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/cpo/2.2/tariffs`,
            },
            {
              identifier: 'commands',
              role: 'RECEIVER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/cpo/2.2/commands`,
            },
          ],
        },
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/cpo/2.2/locations', CpoBackend.authorize, async (req, res) => {
      res.links({
        next: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/cpo/2.2/locations`,
      }).set({
        'X-Limit': '1',
        'X-Total-Count': '1',
      }).send({
        status_code: 1000,
        data: cpoData.locations.map((loc) => this.changeOwner(loc)),
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/cpo/2.2/locations/:id', CpoBackend.authorize, async (req, res) => {
      const location = cpoData.locations.find((loc) => loc.id === req.params.id);
      if (location) {
        res.send({
          status_code: 1000,
          data: this.changeOwner(location),
          timestamp: new Date(),
        });
      } else {
        res.send({
          status_code: 2003,
          status_message: 'Location not found',
          timestamp: new Date(),
        });
      }
    });

    this.app.get('/ocpi/cpo/2.2/locations/:id/:evse', CpoBackend.authorize, async (req, res) => {
      const location = cpoData.locations.find((loc) => loc.id === req.params.id);
      if (location) {
        const evseData = location.evses.find((evse) => evse.uid === req.params.evse);
        if (evseData) {
          res.send({
            status_code: 1000,
            data: evseData,
            timestamp: new Date(),
          });
        } else {
          res.send({
            status_code: 2003,
            status_message: 'EVSE not found',
            timestamp: new Date(),
          });
        }
      } else {
        res.send({
          status_code: 2003,
          status_message: 'Location not found',
          timestamp: new Date(),
        });
      }
    });

    this.app.get('/ocpi/cpo/2.2/locations/:id/:evse/:connector', CpoBackend.authorize, async (req, res) => {
      const location = cpoData.locations.find((loc) => loc.id === req.params.id);
      if (location) {
        const evseData = location.evses.find((evse) => evse.uid === req.params.evse);
        if (evseData) {
          const connector = evseData.connectors.find((c) => c.id === req.params.connector);
          if (connector) {
            res.send({
              status_code: 1000,
              data: connector,
              timestamp: new Date(),
            });
          } else {
            res.send({
              status_code: 2003,
              status_message: 'Connector not found',
              timestamp: new Date(),
            });
          }
        } else {
          res.send({
            status_code: 2003,
            status_message: 'EVSE not found',
            timestamp: new Date(),
          });
        }
      } else {
        res.send({
          status_code: 2003,
          status_message: 'Location not found',
          timestamp: new Date(),
        });
      }
    });

    this.app.get('/ocpi/cpo/2.2/tariffs', CpoBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: cpoData.tariffs.map((tariff) => this.changeOwner(tariff)),
        timestamp: new Date(),
      });
    });

    this.app.post('/ocpi/cpo/2.2/commands/:command', CpoBackend.authorize, async (req, res) => {
      setTimeout(async () => {
        console.log(`CPO [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] sending async ${req.params.command} response`);

        const headers = {
          'Content-Type': 'application/json',
          Authorization: `Token ${this.readTokenC()}`,
          'x-request-id': '123',
          'x-correlation-id': '123',
          'ocpi-from-country-code': this.partyInfo.countryCode,
          'ocpi-from-party-id': this.partyInfo.partyID,
          'ocpi-to-country-code': req.headers['ocpi-from-country-code'],
          'ocpi-to-party-id': req.headers['ocpi-from-party-id'],
        };
        const resultBody = { result: 'ACCEPTED' };

        await fetch(req.body.response_url, {
          method: 'POST',
          headers,
          body: JSON.stringify(resultBody),
        });

        if (req.params.command === 'STOP_SESSION') {
          setTimeout(async () => {
            console.log(`CPO [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] sending cdr after session end`);

            const body = this.changeOwner(cpoData.cdrs);
            const remark = {
              MSP_to_bill: {
                country_code: req.headers['ocpi-from-country-code'],
                party_id: req.headers['ocpi-from-party-id'],
              },
            };
            body.remark = JSON.stringify(remark);

            const postCdrResult = await fetch(`${this.partyInfo.node}/ocpi/receiver/2.2/cdrs`, {
              method: 'POST',
              headers,
              body: JSON.stringify(body),
            });

            const getCdrResult = await fetch(postCdrResult.headers.get('location'), { headers });

            const storedCdr = await getCdrResult.json();

            if (storedCdr.status_code === 1000 && storedCdr.data.id === cpoData.cdrs.id) {
              console.log(`CPO [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] acknowledges cdr correctly stored on EMSP system`);
            } else {
              console.log(`CPO [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] could not verify cdr correctly stored on EMSP system`);
            }
          }, 5 * 1000);
        }
      }, 5 * 1000);
      res.send({
        status_code: 1000,
        data: {
          result: 'ACCEPTED',
          timeout: 20,
        },
        timestamp: new Date(),
      });
    });
  }
};
