/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
const fetch = require('node-fetch');
const BackendBase = require('./backend-base');

module.exports = class BillingBackend extends BackendBase {
  static get TOKEN_B() {
    return 'abc-123';
  }

  constructor(partyInfo) {
    super(partyInfo);
    this.initAppRoutes();
  }

  static authorize(req, res, next) {
    if (req.headers.authorization !== `Token ${BillingBackend.TOKEN_B}`) {
      return res.status(401).send({
        status_code: 2001,
        timestamp: new Date(),
      });
    }
    next();
    return null;
  }

  initAppRoutes() {
    this.app.get('/ocpi/versions', BillingBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: [{
          version: '2.2',
          url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/2.2`,
        }],
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/2.2', BillingBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: {
          version: '2.2',
          endpoints: [
            {
              identifier: 'cdrs',
              role: 'RECEIVER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/billing/2.2/cdrs`,
            },
            {
              identifier: 'chbil-invoicing',
              role: 'SENDER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/billing/2.2/invoicing`,
            },
          ],
        },
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/billing/2.2/cdrs/:cdrID', BillingBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        timestamp: new Date(),
      });
    });

    this.app.post('/ocpi/billing/2.2/cdrs', BillingBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        timestamp: new Date(),
      });
      const cdr = req.body;
      const mspInfo = JSON.parse(cdr.remark);
      console.log(`${this.partyInfo.type} [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] received CDR and sending invoice via custom module`);
      await this.sendInvoice(cdr, mspInfo);
    });
  }

  async sendInvoice(cdr, mspInfo) {
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Token ${this.readTokenC()}`,
      'x-request-id': '123',
      'x-correlation-id': '123',
      'ocpi-from-country-code': this.partyInfo.countryCode,
      'ocpi-from-party-id': this.partyInfo.partyID,
      'ocpi-to-country-code': mspInfo.MSP_to_bill.country_code,
      'ocpi-to-party-id': mspInfo.MSP_to_bill.party_id,
    };
    const invoice = {
      date: cdr.end_date_time,
      item_description: `EV charging via ${cdr.country_code} ${cdr.party_id}`,
      quantity: cdr.total_energy.toString(),
      amount_due: cdr.total_cost.excl_vat.toString(),
    };
    await fetch(`${this.partyInfo.node}/ocpi/custom/receiver/chbil-invoicing`, {
      method: 'POST',
      headers,
      body: JSON.stringify(invoice),
    });
  }
};
