/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

const bodyParser = require('body-parser');
const express = require('express');
const fetch = require('node-fetch');
const fs = require('fs');
const morgan = require('morgan');

module.exports = class BackendBase {
  constructor(partyInfo) {
    this.partyInfo = partyInfo;
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(morgan(`${partyInfo.type} [${partyInfo.countryCode} ${partyInfo.partyID}] -- :method :url :status :res[content-length] - :response-time ms`));
  }

  async start() {
    return new Promise((resolve) => {
      this.app.listen(this.partyInfo.backendPort, () => {
        console.log(`${this.partyInfo.type} [${this.partyInfo.countryCode} ${this.partyInfo.partyID}] listening on ${this.partyInfo.backendPort}`);
        resolve();
      });
    });
  }

  saveTokenC(token) {
    fs.writeFileSync(`./${this.partyInfo.countryCode}-${this.partyInfo.partyID}-TOKEN_C`, token);
  }

  readTokenC() {
    return fs.readFileSync(`./${this.partyInfo.countryCode}-${this.partyInfo.partyID}-TOKEN_C`);
  }

  async register(registry, key, tokenB) {
    const partyInfo = this.partyInfo;

    // Register to OCN Registry (if not already)
    const party = await registry.getPartyByOcpi(partyInfo.countryCode, partyInfo.partyID);
    if (!party || party.node.url === '') {
      // add party to registry
      await registry.setPartyRaw(partyInfo.countryCode, partyInfo.partyID, partyInfo.roles, partyInfo.operator, key);
      console.log(`${partyInfo.type} [${partyInfo.countryCode} ${partyInfo.partyID}] written into OCN Registry with OCN node ${partyInfo.node}`);
    } else {
      console.log(`${partyInfo.type} [${partyInfo.countryCode} ${partyInfo.partyID}] has already registered to OCN Registry. Skipping...`);
    }

    // Register party to OCN Node (if not already)
    const regCheckRes = await fetch(`${partyInfo.node}/admin/connection-status/${partyInfo.countryCode}/${partyInfo.partyID}`, {
      headers: {
        Authorization: 'Token randomkey',
      },
    });

    const regCheckText = await regCheckRes.text();

    if (regCheckRes.status !== 200 || regCheckText !== 'CONNECTED') {
      // Request TOKEN_A via OCN Node admin panel
      const adminRes = await fetch(`${partyInfo.node}/admin/generate-registration-token`, {
        method: 'POST',
        headers: {
          Authorization: 'Token randomkey',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify([{ country_code: partyInfo.countryCode, party_id: partyInfo.partyID }]),
      });
      const adminBody = await adminRes.json();

      // Get list of OCPI versions supported by OCN Node
      const versionRes = await fetch(adminBody.versions, {
        headers: {
          Authorization: `Token ${adminBody.token}`,
        },
      });

      const versionBody = await versionRes.json();

      // Get and store v2.2 endpoints of OCPI module interfaces supported by OCN Node
      const versionDetailRes = await fetch(versionBody.data.find((v) => v.version === '2.2').url, {
        headers: {
          Authorization: `Token ${adminBody.token}`,
        },
      });

      const versionDetailBody = await versionDetailRes.json();
      this.nodeEndpoints = versionDetailBody.data.endpoints;

      // Register to OCN Node using OCPI credentials module
      const regRes = await fetch(`${partyInfo.node}/ocpi/2.2/credentials`, {
        method: 'POST',
        headers: {
          Authorization: `Token ${adminBody.token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: tokenB,
          url: `http://172.16.238.30:${partyInfo.backendPort}/ocpi/versions`,
          roles: [{
            party_id: partyInfo.partyID,
            country_code: partyInfo.countryCode,
            role: 'CPO',
            business_details: {
              name: 'Test CPO',
            },
          }],
        }),
      });

      const regBody = await regRes.json();
      this.saveTokenC(regBody.data.token);

      console.log(`${partyInfo.type} [${partyInfo.countryCode} ${partyInfo.partyID}] completed OCPI connection with OCN node at ${partyInfo.node}`);
    } else {
      console.log(`${partyInfo.type} [${partyInfo.countryCode} ${partyInfo.partyID}] has already connected to OCN node at ${partyInfo.node}. Skipping...`);
    }
  }
};
