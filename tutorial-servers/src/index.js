/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
const fetch = require('node-fetch');
const ethers = require('ethers');
const { Registry } = require('@shareandcharge/ocn-registry');
const { Permissions } = require('@shareandcharge/ocn-registry');
const CpoBackend = require('./cpo-backend');
const MspBackend = require('./msp-backend');
const BillingBackend = require('./billing-backend');

const SPENDER = '0x1c3e5453c0f9aa74a8eb0216310b2b013f017813a648fce364bf41dbc0b37647';

const nodes = [
  {
    privateKey: '0x1c3e5453c0f9aa74a8eb0216310b2b013f017813a648fce364bf41dbc0b37647',
    domain: 'http://172.16.238.20:8080',
  },
  {
    privateKey: '0x0dbbe8e4ae425a6d2687f1a7e3ba17bc98c673636790f1b8ad91193c05875ef1',
    domain: 'http://172.16.238.21:8081',
  },
];

const cpoInfo = [
  {
    type: 'CPO',
    partyID: 'CPO',
    countryCode: 'DE',
    roles: [0],
    operator: '0x9bC1169Ca09555bf2721A5C9eC6D69c8073bfeB4',
    node: 'http://172.16.238.20:8080',
    backendPort: '3100',
  },
  {
    type: 'CPO',
    partyID: 'CPX',
    countryCode: 'NL',
    roles: [0],
    operator: '0xC5fdf4076b8F3A5357c5E395ab970B5B54098Fef',
    node: 'http://172.16.238.21:8081',
    backendPort: '3101',
  },
];

function createWallet() {
  return ethers.Wallet.createRandom();
}

async function main() {
  const registry = new Registry('local', SPENDER);
  const permissions = new Permissions('local', SPENDER);

  // Register OCN Node Operator(s)
  await Promise.all(nodes.map(async (node) => {
    const healthCheck = await fetch(`${node.domain}/health`);
    if (healthCheck.status !== 200) {
      throw Error(`Node ${node.domain} not yet healthy`);
    }
    const wallet = new ethers.Wallet(node.privateKey);
    const exists = await registry.getNode(wallet.address);
    if (!exists) {
      await registry.setNodeRaw(node.domain, node.privateKey);
    }
  }));

  /* eslint-disable no-await-in-loop */
  // Setup each of the CPOs
  for (const cpo of cpoInfo) {
    const cpoBackend = new CpoBackend(cpo);
    cpoBackend.start();
    let key;
    if (cpo.partyID === 'CPX') {
      key = SPENDER; // Need to use SPENDER because createAgreementRaw doesn't yet work
    } else {
      key = createWallet().privateKey;
    }
    await cpoBackend.register(registry, key, CpoBackend.TOKEN_B);
  }
  /* eslint-disable no-await-in-loop */

  // Setup the billing service
  const billingServiceInfo = {
    type: 'OTHER',
    partyID: 'BIL',
    countryCode: 'CH',
    roles: [5],
    operator: '0xC5fdf4076b8F3A5357c5E395ab970B5B54098Fef',
    node: 'http://172.16.238.21:8081',
    backendPort: '3102',
  };
  const billingServiceBackend = new BillingBackend(billingServiceInfo);
  await billingServiceBackend.start();
  const wallet = createWallet();
  await billingServiceBackend.register(registry, wallet.privateKey, BillingBackend.TOKEN_B);
  await permissions.setServiceRaw('demo-billing-service', 'billingService.com', [8], wallet.privateKey);

  // createAgreementRaw doesn't seem to work, related to https://bitbucket.org/shareandcharge/ocn-registry/issues/4/set-service-raw-command-fails
  await permissions.createAgreement(wallet.address);

  // Run the EMSP backend which will provide the version endpoints
  // for the OCN Node during the credentials handshake/registration
  const mspInfo = {
    type: 'EMSP',
    partyID: 'MSP',
    countryCode: 'DE',
    roles: [1],
    operator: '0x9bC1169Ca09555bf2721A5C9eC6D69c8073bfeB4',
    node: 'http://172.16.238.20:8080',
    backendPort: '3002',
  };
  const mspBackend = new MspBackend(mspInfo);
  await mspBackend.start();

  // Check MSP connection/registration status
  const msp = await registry.getPartyByOcpi('DE', 'MSP');
  const mspRegCheck = await fetch('http://172.16.238.20:8080/admin/connection-status/DE/MSP', {
    headers: {
      Authorization: 'Token randomkey',
    },
  });
  const mspRegCheckText = await mspRegCheck.text();
  console.log(`${mspInfo.type} [${mspInfo.countryCode} ${mspInfo.partyID}] connection status: [${(msp && msp.node.url !== '') ? 'x' : ' '}] OCN Registry [${mspRegCheckText === 'CONNECTED' ? 'x' : ' '}] OCN Node`);

  setTimeout(() => console.log('Network is ready!'), 1000);
}

main();
