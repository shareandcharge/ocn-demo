/*
    Copyright 2019-2020 eMobilify GmbH

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/
const fetch = require('node-fetch');
const BackendBase = require('./backend-base');

module.exports = class MspBackend extends BackendBase {
  static get TOKEN_B() {
    return 'abc-123';
  }

  constructor(partyInfo) {
    super(partyInfo);
    this.initAppRoutes();
  }

  static authorize(req, res, next) {
    if (req.headers.authorization !== `Token ${MspBackend.TOKEN_B}`) {
      return res.status(401).send({
        status_code: 2001,
        timestamp: new Date(),
      });
    }
    next();
    return null;
  }

  initAppRoutes() {
    this.app.get('/ocpi/versions', MspBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: [{
          version: '2.2',
          url: 'http://172.16.238.30:3002/ocpi/2.2',
        }],
        timestamp: new Date(),
      });
    });

    this.app.get('/ocpi/2.2', MspBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: {
          version: '2.2',
          endpoints: [
            {
              identifier: 'cdrs',
              role: 'RECEIVER',
              url: 'http://172.16.238.30:3002/ocpi/emsp/2.2/cdrs',
            },
            {
              identifier: 'commands',
              role: 'SENDER',
              url: 'http://172.16.238.30:3002/ocpi/emsp/2.2/commands',
            },
            {
              identifier: 'chbil-invoicing',
              role: 'RECEIVER',
              url: `http://172.16.238.30:${this.partyInfo.backendPort}/ocpi/emsp/2.2/invoicing`,
            },
          ],
        },
        timestamp: new Date(),
      });
    });

    this.app.post('/ocpi/emsp/2.2/commands/:command/:uid', MspBackend.authorize, async (req, res) => {
      console.log(`EMSP [DE MSP] received async command response: ${JSON.stringify(req.body)}`);
      res.send({
        status_code: 1000,
        timestamp: new Date(),
      });
    });

    let cdr;

    this.app.get('/ocpi/emsp/2.2/cdrs/20', MspBackend.authorize, async (req, res) => {
      res.send({
        status_code: 1000,
        data: cdr,
        timestamp: new Date(),
      });
    });

    this.app.post('/ocpi/emsp/2.2/cdrs', MspBackend.authorize, async (req, res) => {
      cdr = req.body;
      res.set({
        Location: 'http://172.16.238.30:3002/ocpi/emsp/2.2/cdrs/20',
      }).send({
        status_code: 1000,
        timestamp: new Date(),
      });
    });

    this.app.post('/ocpi/emsp/2.2/invoicing', MspBackend.authorize, async (req, res) => {
      console.log('EMSP [DE MSP] received invoice from billing service');
      res.send({
        status_code: 1000,
        timestamp: new Date(),
      });
    });
  }

  async startSession() {
    console.log('starting sesssion');
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Token ${this.readTokenC()}`,
      'x-request-id': '123',
      'x-correlation-id': '123',
      'ocpi-from-country-code': this.partyInfo.countryCode,
      'ocpi-from-party-id': this.partyInfo.partyID,
      'ocpi-to-country-code': 'NL',
      'ocpi-to-party-id': 'CPX',
    };
    const body = {
      response_url: 'http://172.16.238.30:3002/ocpi/emsp/2.2/commands/START_SESSION/1',
      token: {
        country_code: 'DE',
        party_id: 'MSP',
        uid: '0102030405',
        type: 'APP_USER',
        contract_id: 'XX-12345',
        issuer: 'Test MSP',
        valid: true,
        whitelist: 'ALWAYS',
        last_updated: '2019-08-13T14:44:25.561Z',
      },
      location_id: 'LOC1',
    };

    const asyncResult = await fetch(`${this.partyInfo.node}/ocpi/receiver/2.2/commands/START_SESSION`, {
      method: 'POST',
      headers,
      body: JSON.stringify(body),
    });
    console.log('started session');
    return asyncResult;
  }

  async stopSession() {
    console.log('stopping sesssion');
    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Token ${this.readTokenC()}`,
      'x-request-id': '123',
      'x-correlation-id': '123',
      'ocpi-from-country-code': this.partyInfo.countryCode,
      'ocpi-from-party-id': this.partyInfo.partyID,
      'ocpi-to-country-code': 'NL',
      'ocpi-to-party-id': 'CPX',
    };
    const body = {
      response_url: 'http://172.16.238.30:3002/ocpi/emsp/2.2/commands/STOP_SESSION/2',
      session_id: 'xyzxyzyxyz',
    };

    const asyncResult = await fetch(`${this.partyInfo.node}/ocpi/receiver/2.2/commands/STOP_SESSION`, {
      method: 'POST',
      headers,
      body: JSON.stringify(body),
    });
    console.log('stopped session');
    return asyncResult;
  }
};
