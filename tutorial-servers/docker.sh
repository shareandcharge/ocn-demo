#!/usr/bin/env bash

until npm start 2> /dev/null; [ $? -ne 0 ]; do
    echo "Waiting for network..."
    sleep 1
done
