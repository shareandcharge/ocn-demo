# Open Charge Network (OCN) Tutorial

The following tutorial will serve as a gentle introduction to the OCN. We have 
automated the setup of the network, but it should provide an entry point for 
those looking to go deeper. For OCPI developers, this tutorial aims to showcase
the benefits of the OCN in managing connections with other parties, as well as 
the potential for services built on top of the OCN in the future.

# Setup

## Installing Dependencies

Before you start, you should download the files in this directory (i.e by 
`git clone`), paying attention to the branch you are in. 

### Docker

[Docker](https://docs.docker.com/install/) and 
[docker-compose](https://docs.docker.com/compose/install/) are required to run
the local network.

### OCN Registry CLI

The [OCN Registry](https://bitbucket.org/shareandcharge/ocn-registry) CLI
should be installed, for example via [NPM](https://www.npmjs.com/get-npm):

```
npm install -g @shareandcharge/ocn-registry@rc
```

### Postman

[Postman](https://www.getpostman.com/) will also be used to send HTTP requests.
Once installed, simply import the JSON collection and environment files provided
in this directory. See the 
[Postman documentation](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/#importing-postman-data)
if you are unsure how to do this.

Once imported, the provided "OCN Node" environment should be selected from 
the drop-down menu in the top right corner of Postman. This will allow you to 
easily change common variables across all your future requests. More details
can be found [here](https://learning.postman.com/docs/sending-requests/managing-environments/)
if you need help managing environments in Postman.


## Running your Local Open Charging Network

Simply run the following command in the same directory as the 
`docker-compose.yml` file. Note that you may have to run the command with 
`sudo` privileges, depending on your system.

```
docker-compose up
```

The setup will take a short while to complete. You should see the following
from Docker once done:

```
ocn-demo-servers        | Network is ready!
```

To test our connection to the nodes, we can visit http://localhost:8080/health
and http://localhost:8081/health in our browser. Both should return OK
responses.

To restart the network at any time, simply `Ctrl+C` and `docker-compose up` 
again.

# Tutorial

In this tutorial, we will assume the role of an eMSP.

The tutorial assumes no prior knowledge of OCPI. Where necessary, the official
[OCPI 2.2 documentation](https://github.com/ocpi/ocpi) can be consulted for 
more detailed information.

The following steps outline the tutorial:

1. [Using the OCN Node API](./tutorial-steps/1-using-node-api)
2. [Using the OCN Registry](./tutorial-steps/2-using-the-registry)
3. [Requesting Token A](./tutorial-steps/3-requesting-token-a)
4. [Requesting OCPI Versions](./tutorial-steps/4-requesting-ocpi-versions)
5. [Credentials Handshake](./tutorial-steps/5-credentials-handshake)
6. [Requesting Location data from a CPO](./tutorial-steps/6-requesting-location-data)
7. [Choosing the recipient](./tutorial-steps/7-choosing-recipients)
8. [Understanding OCPI module dependencies](./tutorial-steps/8-module-dependencies)
9. [Sending asynchronous requests](./tutorial-steps/9-async-requests)
10. [Using the Service Interface](./tutorial-steps/10-billing-service)

You can get started right away, or read over the components below
to get a better understanding of what exactly is included in the network.

## Network Components

The following information explains the makeup of the network once running. 
The provided YAML file also contains the configuration of the network, for a 
tl;dr version.

### A blockchain

This is an instance of [ganache](https://github.com/trufflesuite/ganache-cli)
with a JSON RPC API available at http://localhost:8544.

This blockchain only runs locally, and is intended for development purposes. 
Provided is a wallet containing 20 addresses each pre-funded with 100 ETH 
(the currency of this network). 

The wallet mnemonic (used to re-create the private keys in the wallet) is:
```
candy maple cake sugar pudding cream honey rich smooth crumble sweet treat
```

**Disclaimer: don't use any of these keys outside of your local development
environment. We are not responsible for any monetary loss occurred by using
them.**

### Smart Contracts

The [OCN Registry](https://bitbucket.org/shareandcharge/ocn-registry) smart 
contracts hold the state of participants on the network. Additionally, OCN 
Services can extend the functionality of OCPI through the permissions contract.
More on that later.

The contracts have been deployed by the first private key in the wallet stated
above. This private key (the owner of the contracts) has the address:
```
0x627306090abaB3A6e1400e9345bC60c78a8BEf57
```

The following contracts have addresses which are used to identify them on 
the network:

- Registry: `0x345cA3e014Aaf5dcA488057592ee47305D9B3e10`
- Permissions: `0xf25186B5081Ff5cE73482AD761DB0eB0d25abfBF`

### OCN Nodes

Two nodes exist on this network, with HTTP APIs (including OCPI) at 
http://localhost:8080 and http://localhost:8081 respectively. The admin API
key is the same for each: `randomkey`. 

Both nodes have already been added to the Registry smart contract. You
can query that with the following command (via the OCN Registry CLI):

```
ocn-registry list-nodes
```

### Charge Point Operators and Billing Service

There are two CPOs registered on the network, one on each node. 
They have added themselves to the OCN Registry and have completed the credentials 
handshake with their respective nodes.
A billing service is also registered on the network.
The code for the CPOs and the billing service can be found in the [tutorial-servers](./tutorial-servers) directory