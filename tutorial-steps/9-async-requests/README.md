# Step 9: Asynchronous Requests

Following on from the discussion in [step 8](../8-module-dependencies) 
concerning module dependencies, we will now demonstrate everything we have 
learnt so far, in addition to introducing asynchronous requests, as well as
the OCPI push model.

Our eMSP has actually implemented two OCPI modules: `commands` and `cdrs`.

The first of which is the `SENDER` interface, allowing the CPO to 
asynchronously notify the eMSP of a command result, i.e. a session has been 
started by a charge point.

The second allows the CPO to push Charge Detail Records (CDRs), containing the
final price of a charging session, to the eMSP. This is an example of the push
model in OCPI, which reduces the load on the CPO backend as the eMSP doesn't 
need to constantly check for new CDRs. Of course, it is up to the CPO to
implement this logic in their backend.

We can see this in action by sending the `OCPI / Commands / POST START_SESSION`
request in Postman, which will tell the CPO a driver on the eMSP system wishes
to start a session remotely. 

For this request, we can also monitor the output of our demo servers. The 
initial response from our Postman request contains only an acknowledgement 
of the request. After 5 seconds the CPO will send the async command result to
the eMSP server:

```
CPO [DE CPO] sending async START_SESSION response
EMSP [DE MSP] received async command response: {"result":"ACCEPTED"}
EMSP [DE MSP] -- POST /ocpi/emsp/2.2/commands/START_SESSION/1 200 59 - 0.734 ms
```

Likewise, when we make the `OCPI / Commands / POST STOP_SESSION` request, we 
see a similar pattern, with the command result sent to the eMSP backend
5 seconds after the initial confirmation.

After another 5 seconds, the CPO sends a CDR to the eMSP, containing information
about the price of the charging session:

```
CPO [DE CPO] sending cdr after session end
EMSP [DE MSP] -- POST /ocpi/emsp/2.2/cdrs 200 59 - 0.487 ms
EMSP [DE MSP] -- GET /ocpi/emsp/2.2/cdrs/1 200 1124 - 0.784 ms
CPO [DE CPO] acknowledges cdr correctly stored on EMSP system
```

The eMSP responds to the CPO with a RESTful `Location` header, describing where 
the CPO can find this charge detail record on the eMSP system. The CPO then 
makes a GET request to this location to verify that the CDR was stored correctly
by the eMSP. 

The final part of the tutorial showcases two additional features of the OCN,
the service interface and custom OCPI modules.

[>> Step 10](../10-billing-service)