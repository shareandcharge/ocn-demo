# Step 7: Choosing the Recipient

Any request that is meant for another party on the network needs to have
routing headers. Such headers are prefixed with `OCPI-` and tell the node who
is sending the request, and who should receive it. 

When we made our requests in [step 6](../6-requesting-location-data), we were
using a pre-configured recipient. This can be found in the "Headers" tab of the
requests in Postman.

The `OCPI-from-country-code` and `OCPI-from-party-id` describe the party
making the request (on our eMSP platform we only have one party, but it 
could be the case that, in OCPI 2.2, a CPO and eMSP party share the same 
connection with an OCN node). 

Likewise, the `OCPI-to-country-code` and `OCPI-to-party-id` headers describe 
the recipient of the request. Here we can see that we were wished for the
request to be forwarded to a party with country code "DE" and party ID "CPO".

This CPO is using the same node as our eMSP, but what if we want to 
contact a "remote" party connected to a different OCN Node? In the registry 
there was another CPO:

```
OCPI-to-country-code: NL
OCPI-to-party-id: CPX
```

Note also the `X-Request-ID` and `X-Correlation-ID` headers. They don't play a
role in our demonstration, but in production it is strongly advised to generate
unique IDs (e.g. uuid version 4) for all requests, in order to help with
debugging. 

See [Unique Message IDs](https://github.com/ocpi/ocpi/blob/master/transport_and_format.asciidoc#12-unique-message-ids) 
in the OCPI 2.2 documentation for further information.

Try modifying the headers in Postman to send the request to the different CPO.
If you view the logs of our Docker network, we should see a confirmation
from the CPO in Netherlands that it received the request.

In the next step, we will shortly discuss OCPI module dependencies.

[>> Step 8](../8-module-dependencies)
