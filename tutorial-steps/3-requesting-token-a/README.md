# Step 3. Requesting a Token A

Now we have listed ourselves in the registry (as in 
[step 2](../2-using-the-registry)), we now can to create a connection
with our node. 

To do so, we must obtain a `CREDENTIALS_TOKEN_A` to initiate 
the [OCPI registration flow](https://github.com/ocpi/ocpi/blob/master/credentials.asciidoc#111-registration).

In Postman, simply send the `Admin / GET Generate Registration Token` request 
in our collection. As we are the administrator of our nodes, we have access to
this facility. 

Note that on a remote node, we may need to contact the operator to get our 
token.

You should see the following response:
```
{
    "token": {{CREDENTIALS_TOKEN_A}},
    "versions": "http://localhost:8080/ocpi/versions"
}
```

The token can now be applied to our `CREDENTIALS_TOKEN_A` environment variable
for future requests. If you are unsure how to set environment variables, see
[Adding environment variables](https://learning.postman.com/docs/postman/variables-and-environments/managing-environments/#adding-environment-variables).

We can now use this token to request version details, including the endpoints
supported by the OCN Node. Once we have done this, we can initiate the 
credentials handshake to receive our Token C. This will be used to send
requests to other parties on the network.

[>> Step 4](../4-requesting-ocpi-versions)
