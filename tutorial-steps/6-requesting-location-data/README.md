# Step 6: Making OCPI requests to a CPO

Now we have registered with our node, we can send requests to the CPOs 
on the OCN.

When we queried the registry, we saw two CPOs already registered on the
network. 

The `OCPI / Locations / GET locations list` request in our Postman collection
will fetch a list of one of the CPO's locations (i.e. charging stations under 
OCPI terminology).

We should see a success response after sending the request, with `status_code` 
1000, returning an array of a single location. The OCPI location data type 
follows a hierarchy of `location` -> `evse` -> `connector`. 

We can make also make requests that fetch a single location, EVSE or 
connector. Take a look at the other requests in the folder to see how they
work. 

These requests are all pre-configured to be sent to a particular CPO. What
if we want to contact a different party? The next step will show you how.

[>> Step 7](../7-choosing-recipients)
