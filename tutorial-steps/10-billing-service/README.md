# Step 10: Using a billing service

This step introduces two new features of the OCN: the service interface and custom modules.

The service interface allows service providers to register as parties on OCN. 
A different party can then agree to the permissions requirements of a service provider. 
This enables the OCN nodes to forward OCPI requests of the party to the service provider.
Other types of permissions and corresponding actions may be added in the future.

Custom modules are a part of the OCPI spec. They allow for parties to exchange custom
messages using OCPI, as long as they have pre-agreeded to use the module.

We will now repeat the scenario from the previous step involving the `commands`
and `cdrs` OCPI module but this time we will add in interaction with a billing
service. The billing service requires the `FORWARD_MODULE_CDRS_RECEIVER` permission.
This permission means that all requests sent by a party to the OCPI receiver interface
will be forwarded to the billing service.

A CPO, different from the one the previous step, has already agreed to the permissions required
by the billing service.
The MSP and billing service have implemented an "invoicing" custom module.

The interaction with the billing service in our scenario will be as follows:
1. The CDR sent from the CPO to the eMSP will be forwarded to the billing service
2. The billing service will use the CDR to send the eMSP an invoice via a custom module

For a step by step diagrammed outline of the interaction flow, check-out this [Medium article](https://medium.com/share-charge/third-party-services-for-the-ocpi-community-a-billing-example-3c168177c0a2).

We can trigger this interaction by sending the `OCPI / Commands / POST START_SESSION`, followed
by the `OCPI / Commands / POST STOP_SESSION` request in Postman, with modified headers.
For each of the two requests, modify the headers as follows:

- OCPI-to-country-code -> change from `DE` to `NL`

- OCPI-to-party-id -> change from `CPO` to `CPX`

After sending the sequence of START_SESSION and STOP_SESSION responses,
when the CDR is send to the eMSP you should see that it is also being forwarded
to the billing service (the order may be reversed):
```
EMSP [DE MSP] -- POST /ocpi/emsp/2.2/cdrs 200 59 - 0.688 ms
```
and
```
OTHER [CH BIL] received CDR and sending invoice via custom module
OTHER [CH BIL] -- POST /ocpi/billing/2.2/cdrs 200 59 - 1.112 ms
```

The eMSP outputs that it has received the invoice:
```
EMSP [DE MSP] received invoice from billing service
EMSP [DE MSP] -- POST /ocpi/emsp/2.2/invoicing 200 59 - 0.701 ms
```

Note that the CDR location verification request is sent to the billing service
as well as the eMSP, because all requests sent by CPO `NL CPX` to the
`cdrs` module RECEIVER interface are forwarded to the billing service:
```
OTHER [CH BIL] -- GET /ocpi/billing/2.2/cdrs/14 200 59 - 0.443 ms
EMSP [DE MSP] -- GET /ocpi/emsp/2.2/cdrs/20 200 1200 - 0.501 ms
CPO [NL CPX] acknowledges cdr correctly stored on EMSP system
```

This marks the final step in the tutorial. Hopefully you now have the building
blocks to start integrating with the Open Charging Network.

As a final remark: the Open Charging Network relies on its community to drive
forward innovation in e-mobility roaming. Do not hesitate to bring forward
issues and suggestions that can help improve the network as a whole.
