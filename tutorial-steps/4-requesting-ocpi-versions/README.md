# Step 4: Request Versions and Version Details

We want our eMSP backend and OCN Node to communicate using the same version of
OCPI. Using the `OCPI / Versions / GET Versions` request in Postman, we can 
find our mutual OCPI version. This request is sent using the "TOKEN_A" we 
received in [step 3](../3-requesting-token-a).

The response from the OCN Node contains the OCPI fields: `status_code`, 
`status_message` (optional), `data` (optional) and `timestamp`. 

Assuming our `status_code` is 1000 (the OCPI success code), the `data` field
should provide an array of supported versions with URLs where we can find 
endpoints.

The request `OCPI / Versions / GET Version Details` in our collection shows us
the different endpoints supported by the OCN Node for OCPI version 2.2. 

The one we are most interested in right now is the `credentials` module, which
allows us to complete our registration:

```json
{
    "identifier": "credentials",
    "role": "SENDER",
    "url": "http://172.16.238.20:8080/ocpi/2.2/credentials"
}
```

We will use this endpoint in the next step, where we initiate the credentials
handshake with our node.

[>> Step 5](../5-credentials-handshake)
