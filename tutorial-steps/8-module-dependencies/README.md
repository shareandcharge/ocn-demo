# Step 8: Understanding OCPI Module Dependencies

Let's check out the other request types we can make. Notice how on the 
Connector objects, received from the CPO in 
[step 6](../6-requesting-location-data), there is a `tariff_ids` array. 
What does this mean? 

The `OCPI / Tariffs / GET tariffs` request in Postman returns an array of 
tariffs provided by the CPO, with IDs matching those found on the Connector 
object. This is an example of a dependency between OCPI modules. 

However, all OCPI modules aside from `credentials` are optional (for the 
purpose of the demo the eMSP and CPOs have not implemented the `credentials` 
interface, but it is important to do so, as the OCN Node could need to update 
its credentials on your system). 

It is up to a party to implement the modules themselves. Therefore, if we try 
to make, for instance, a `sessions` request to the CPO, we might receive an
error message telling us that the CPO has not implemented the module.

In the OCPI documentation for Connectors, we also see that the `tariff_ids`
field is optional. The CPO may have only implemented locations, without any
billing information.

Because of this modularity in the design of OCPI, it is very important to pay
attention to such intricacies. The OCN Node is stateless; it will only
evaluate the validity of the current OCPI request and will not recall its 
context. 

In a situation where, for example, an optional field is missing
from a request when it should be present (based on previous correspondence), 
contact should be made with the relevant counter-party to address this.

The next part of the tutorial ties everything together with the use of
asynchronous requests.

[>> Step 9](../9-async-requests)
