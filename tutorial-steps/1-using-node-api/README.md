# Step 1: Getting the ID of a Node

The OCN Node provides, alongside a full OCPI 2.2 API, a few other HTTP 
endpoints for administrating and querying certain aspects of the OCN.

For the purpose of the tutorial, we would like to verify the unique identifier
of the node we will be using. For other request types, check out the 
[OCN Node API documentation](https://shareandcharge.bitbucket.io).

Before we create a connection to an OCN Node, we must enter our details into
the [OCN Registry](https://bitbucket.org/shareandcharge/ocn-registry). To do
this, we must sign a transaction which states our party's country code and 
party ID as well as the address (ID) of the OCN Node we want to connect to. 

Provided in the Postman collection (in the parent directory) is the request 
`OCN / GET Node Info`. 

The environment variable `NODE_URL` in the URL has already been configured to
`http://localhost:8080` within the "OCN Node" environment (also in the 
parent directory if not already imported). 

Sending this request should provide the following response:

```json
{
    "url": "http://172.16.238.20:8080",
    "address": "0x9bc1169ca09555bf2721a5c9ec6d69c8073bfeb4"
}
```

This is the public IP of the container within our Docker network, accessible to 
the other containers in our tutorial. The container's 8080 port has been bound 
to the 8080 port on localhost for our convenience. Ordinarily, however, we would 
see here a publicly accessible host or domain name.

You may have seen the same information printed in the Docker logs when you 
started the network. As we are administrating our nodes, we already have access
to this information (set in our docker-compose config). 

If connecting to a remote node, however, we should make sure we have the correct
address. We can also verify this information in the registry, just to be sure:

```
ocn-registry list-nodes
```

Now we have the address, it can be used as an identifier when expressing our
use of this particular node in the registry.

[>> Step 2](../2-using-the-registry)
