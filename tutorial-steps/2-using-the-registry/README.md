# Step 2: Adding a Party to the OCN Registry

Assuming we have our node's unique identifier (found in 
[step 1](../1-using-node-api)), we can now add our party to the registry:

```
ocn-registry set-party --credentials DE MSP \
    --roles EMSP \
    --operator 0x9bc1169ca09555bf2721a5c9ec6d69c8073bfeb4 \
    --signer 0x49b2e2b48cfc25fda1d1cbdb2197b83902142c6da502dcf1871c628ea524f11b
```

You should see a transaction object which contains a hash. Such a hash can be
used to verify the integrity of any transaction on a network. 

Our new eMSP has country code "DE" and party ID "MSP". We have a single OCPI 
role (EMSP), and will be connected to the node with address
`0x9bc1169ca09555bf2721a5c9ec6d69c8073bfeb4`. 

The signer is the private key used to sign and pay for the transaction on the
blockchain. As this is a development network we can pre-allocate funds to any
private key we wish.

We can also verify the registry entry with the CLI:
```
ocn-registry list-parties
```
You should now see our eMSP listing along with the other parties on the network.

Now we can begin our credentials registration with the OCN Node, beginning
with requesting a "TOKEN_A".

[>> Step 3](../3-requesting-token-a)