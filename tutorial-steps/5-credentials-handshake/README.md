# Step 5: The credentials handshake

The `OCPI / Credentials / POST Credentials` request in our Postman collection
has already been configured for our use. Our eMSP backend admin discovered the
credentials endpoint in [step 4](../4-requesting-ocpi-versions).

This request contains a JSON body (found under the "Body" tab) with our own 
credentials to be sent to the OCN Node. This includes the token which the OCN
Node needs to authorize itself on *our* server. 

A URL to our versions endpoint will be used by the node to make sure we have
implemented OCPI version 2.2. It will then store the version 2.2 endpoints that
we have described. Our eMSP server running in Docker provides the version
endpoints for us.

We also describe the roles that we employ. Notice how this is an array. OCPI 
2.2 adds the ability for a platform operating multiple roles to communicate on
a single OCPI connection. Therefore, a platform that is both an eMSP and CPO 
needs not register twice.

```json
{
    "status_code": 1000,
    "data": {
        "token": "{{CREDENTIALS_TOKEN_C}}",
        "url": "http://172.16.238.20:8080/ocpi/versions",
        "roles": [
            {
                "role": "HUB",
                "business_details": {
                    "name": "Open Charging Network Node"
                },
                "party_id": "OCN",
                "country_code": "CH"
            }
        ]
    },
    "timestamp": "2020-01-15T09:56:56.547Z"
}
```

Once sent, you should see the OCN Node's credentials returned to you. There is
a new token in the body: our `CREDENTIALS_TOKEN_C` which will be used to 
authorize any subsequent OCPI requests you make to your OCN Node. Make sure to
set it in your environment variables, as the previous token has now been 
invalidated.

This now completes the registration to the OCN Node. We are ready to send
requests to other parties on the network.

[>> Step 6](../6-requesting-location-data)
